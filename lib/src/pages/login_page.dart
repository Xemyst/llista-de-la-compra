
import 'package:flutter/material.dart';
import 'package:llistacompra/src/pages/contador_page.dart';

class LoginPage extends StatelessWidget {
  final estiloTexto = TextStyle(fontSize: 25 ,color: Colors.red);
  final count = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Eric')
      ),
      body: Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[
          Center(child:Text('eric',style: estiloTexto),),
          ContadorPage()
        ],) ,
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.red,
        child: Icon(Icons.add),
//        onPressed: countBoy,
      ),
    );
  }
}