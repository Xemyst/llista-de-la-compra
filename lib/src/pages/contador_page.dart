import 'package:flutter/material.dart';

class ContadorPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ContadorPageState();
}

class _ContadorPageState extends State {
  final _estiloTexto = TextStyle(fontSize: 25 ,color: Colors.red);
  int _count = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Eric')
      ),
      body: Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[
          Center(child:Text('eric',style: _estiloTexto),),
          Text('$_count', style: _estiloTexto)
        ],) ,
      ),
      floatingActionButton: _creatBotones()
    );
  }

  Widget _creatBotones() {
    return Row (
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          SizedBox(width: 30),
          FloatingActionButton(child:Icon(Icons.add),onPressed: countBoy),
          Expanded(child: SizedBox(width: 5,) ),
          FloatingActionButton(child:Icon(Icons.exposure_zero),onPressed: zeroBoy),
          SizedBox(width: 5,),
          FloatingActionButton(child:Icon(Icons.remove),onPressed: restBoy),
    ]);

  }

  void countBoy(){
    _count += 1;
    setState(() {
    });
  }
  void restBoy(){
    _count -= 1;
    setState(() {
    });
  }
  void zeroBoy(){
    _count = 0;
    setState(() {
    });
  }



}