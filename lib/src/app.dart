import 'package:flutter/material.dart';
import 'package:llistacompra/src/pages/contador_page.dart';
import 'package:llistacompra/src/pages/login_page.dart';

class LlistaCompra extends StatelessWidget{

  @override
  Widget build( context ) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
//      debugShowMaterialGrid: true,
      home: Center(
        child: ContadorPage()
      )
    );
  }

}

